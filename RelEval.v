Require Import Coq.Strings.String.
Require Import Coq.Lists.List.
Require Import Coq.Arith.EqNat.
Require Import KanrenSyntax.

Definition Spec := ((list (Def Xe)) * G Xe) % type.


(* TODO: occursCheck? *)
(* TODO: walk !! *)
Inductive MguR : Sigma -> Ts -> Ts -> Sigma -> Prop :=
| MguVar : forall (sigma : Sigma) (u v : Se),
    u = v -> MguR sigma (V Se u) (V Se v) sigma
| MguLeft : forall (sigma : Sigma) (u : Se) (v : Ts),
    MguR sigma (V Se u) v ((u, v) :: sigma)
| MguRight : forall (sigma : Sigma) (u : Ts) (v : Se),
    MguR sigma u (V Se v) ((v, u) :: sigma)
| MguConstr : forall (sigma sigma' : Sigma) (a b : Name) (as' bs' : list (Term Se)),
    a = b -> length as' = length bs' -> MguList sigma as' bs' sigma' ->
    MguR sigma (C Se a as') (C Se b bs') sigma'
with MguList : Sigma -> list Ts -> list Ts -> Sigma -> Prop :=
     | MguListH : forall s as' bs' s' s'' a b,
         Some a = hd_error as' -> Some b = hd_error bs' -> MguR s a b s' ->
         MguList s' (tl as') (tl bs') s'' -> MguList s as' bs' s''
     | MguListT : forall s as' bs', as' = nil -> bs' = nil -> MguList s as' bs' s.

Definition extend_iota_args (iota : Iota) (fs : list Xe) (as' : list Ts) : Iota :=
  fold_left (fun i fa => extend i (fst fa) (snd fa))
            (combine fs as')
            iota.

Definition gamma_iota (gamma : Gamma) := match gamma with
                              | (_,iota,_) => iota
                              end.

Inductive EvalR : Gamma -> Sigma -> G Xe -> list (Sigma * Delta) -> Prop :=
| UnifyFailR : forall gamma sigma sigma' goal (t1 t2 : Tx) (ti1 ti2 : Ts) iota,
    iota = gamma_iota gamma ->
    goal = Uni _ t1 t2 ->
    Some ti1 = (iota <@> t1) ->
    Some ti2 = (iota <@> t2) ->
    ~ (MguR sigma ti1 ti2 sigma') ->
    EvalR gamma sigma goal nil
| UnifySuccessR : forall gamma sigma subst' sigma' goal t1 t2 ti1 ti2 delta p iota,
    goal = Uni _ t1 t2 ->
    Some ti1 = (iota <@> t1) ->
    Some ti2 = (iota <@> t2) ->
    MguR sigma ti1 ti2 subst' ->
    o subst' sigma = Some sigma' ->
    gamma = (p,iota,delta) ->
    EvalR gamma sigma goal ((sigma', delta) :: nil)
| DisjR : forall gamma sigma goal1 subst1 goal2 subst2,
    EvalR gamma sigma goal1 subst1 ->
    EvalR gamma sigma goal2 subst2 ->
    EvalR gamma sigma (Disj _ goal1 goal2) (subst1 ++ subst2)
| ConjR : forall gamma sigma goal1 subst1 goal2 si p iota delta,
    (p, iota, delta) = gamma ->
    EvalR gamma sigma goal1 subst1 ->
    forall i sigmai deltai sigmair,
      Some (sigmai, deltai) = nth_error subst1 i ->
      EvalR (p, iota, deltai) sigmai goal2 sigmair ->
      forall i' sir, Some sir = nth_error sigmair i' ->
      In sir sigmair ->
      EvalR gamma sigma (Conj _ goal1 goal2) si
| FreshR : forall gamma sigma goal delta newiota x p iota subst',
    (p, iota, delta) = gamma ->
    newiota = extend iota x (V _ (new_se delta)) ->
    EvalR (p, newiota, (new_se delta)) sigma goal subst' ->
    EvalR gamma sigma (Fresh _ x goal) subst'
| InvokeR : forall gamma sigma name rname (p : dict (Def Xe)) iota delta fs as' g0 subst' interpreted,
    (p, iota, delta) = gamma ->
    Some (def _ name fs g0) = lookup_dict p rname ->
    Some interpreted = interpret_list iota as' ->
    EvalR (p, (extend_iota_args emptyIota fs interpreted), delta) newsigma g0 subst' ->
    EvalR gamma sigma (Invoke _ rname as') subst'.

Compute option_map
        (fun pre => match pre with
                    | (goal', env', _) => eval env' nil goal' 10
                    end)
        (pre_eval nil env0 x_2_g).
Example x_2_g_r :
  EvalR env0 nil x_2_g (((1, C Se "O" nil) :: nil, 1) :: nil).
Proof.
  remember (extend emptyIota "x" (V _ (new_se 0))) as newiota.
  apply FreshR with (delta := 0) (p := nil) (iota := emptyIota)
                    (newiota := newiota).
  - unfold env0. tauto.
  - tauto.
  - remember (V Xe "x") as t1.
    remember (zero Xe) as t2.
    apply UnifySuccessR with (subst' := (1, C Se "O" nil) ::nil)
                             (t1 := t1) (t2 := t2)
                             (ti1 := V Se 1) (ti2 := C Se "O" nil) (p := nil) (iota := newiota); auto.
    + rewrite Heqnewiota. simpl. unfold interpret. rewrite Heqt1. simpl.
      unfold new_se. tauto.
    + rewrite Heqnewiota. simpl. unfold interpret. rewrite Heqt2. simpl. tauto.
    + apply MguLeft.
Qed.

