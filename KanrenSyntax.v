Require Import Coq.Strings.String.
Require Import Coq.Lists.List.
Require Import Coq.Arith.EqNat.
Require Import Coq.Lists.ListSet.
Import ListNotations.

Definition Xe    := string. (* Syntatic variables *)
Definition Se    := nat.    (* Semantic variables *)
Definition Name := string. (* Names of variables/definitions *)

Definition str_eq (s1: string) (s2: string) : bool :=
  prefix s1 s2 && prefix s2 s1.

Example str_eq_works_same : str_eq "123" "123" = true.
unfold str_eq. auto. Qed.

Example str_eq_works_diffrent : str_eq "123" "321" = false.
unfold str_eq. auto. Qed.


Definition dict A := list (string * A).

Definition extend_dict {A} (m : dict A) (k : string) (v : A) : dict A :=
  (k, v) :: m.

Fixpoint lookup_dict {A} (m : dict A) (l : string) : option A :=
  match m with
  | (k, v) :: tail => if str_eq k l then Some v else (lookup_dict tail l)
  | _ => None
  end.

(* Terms *)
Inductive Term (v : Type) : Type :=
| V : v -> Term v
| C : string -> list (Term v) -> Term v.

Definition Tx := Term Xe.
Definition Ts := Term Se.

Fixpoint beq_term (t1 t2: Ts) (d: nat) : bool :=
  match d with
  | O => false
  | S n => match t1, t2 with
           | V _ t1', V _ t2' => beq_nat t1' t2'
           | C _ t1n' t1t', C _ t2n' t2t' =>
             andb (str_eq t1n' t2n')
                  (forallb (fun c => match c with
                                     | (h, t) => beq_term h t n
                                     end)
                           (combine t1t' t2t'))
           | _, _ => false
           end
  end.

(* Definitions *)

Inductive G (v: Type) : Type :=
| Uni : Term v -> Term v -> G v
| Conj : G v -> G v -> G v
| Disj : G v -> G v -> G v
| Fresh : Name -> (G v) -> G v
| Invoke : Name -> list (Term v) -> G v
| Let : (Def v) -> G v -> G v
with Def (v: Type) : Type :=
     | def : Name -> list Name -> G Xe -> Def v.

Definition Iota  := ((list Xe) * (Xe -> option Ts)) % type.
Definition Sigma := list (Se * Ts) % type.
Definition Delta := Se.
Definition P     := dict (Def Xe).
Definition Gamma := (P * Iota * Delta) % type.

Definition new_se (se : Se) := S se.

Fixpoint lookup (l : Sigma) (el : nat) : option Ts :=
  match l with
  | h :: t => if (beq_nat el (fst h)) then (Some (snd h)) else lookup t el
  | _ => None
  end.

Fixpoint walk (t: Ts) (s: Sigma) (depth: nat) : Ts :=
match depth with
  | O => t
  | S d' => match t with
            | V _ v' => match (lookup s v') with
                        | Some t' => walk t' s d'
                        | None => t
                        end
            | _ => t
            end
end.

Definition newsigma : Sigma := nil.

Fixpoint unify' (subs: Sigma) (u: Ts) (v: Ts) : option Sigma :=
  match u with
  | V _ u' => match v with
              | V _ v' => if (beq_nat u' v')
                          then Some subs
                          else Some ((u', v) :: subs)
            | _ => Some ((u', v) :: subs)
            end
  | C _ a as' => match v with
                 | C _ b bs' =>
                   if (andb (str_eq a b) (beq_nat (length as') (length bs')))
                   then Some nil
                   else None
                 | V _ v' => Some ((v', u) :: subs)
               end
  end.

Definition option_flat_map {A B : Type} (f:A->option B) (o: option A)
  : option B :=
  match (option_map f o) with
  | Some r1 => match r1 with
               | Some r2 => Some r2
               | None => None
               end
  | None => None
  end.

Definition unify (sigma: option Sigma) (u v : Ts) (depth: nat) : option Sigma :=
  option_flat_map(fun s => unify' s (walk u s depth) (walk v s depth)) sigma.

Fixpoint opt_list_to_list_opt {A : Type} (acc : list A) (l : list (option A)) : option (list A) :=
  match l with
  | h :: t => match h with
              | Some s => opt_list_to_list_opt (s :: acc) t
              | None => None
              end
  | nil => Some acc
  end.

Fixpoint interpret (i : Iota) (v : Tx) : option Ts :=
  match v with
  | V _ x => (snd i) x
  | C _ c ts =>
    option_map
      (fun l => C _ c l)
      (opt_list_to_list_opt nil (map (interpret i) ts))
  end.

Notation "A <@> B" := (interpret A B) (at level 100, right associativity).

Definition inb el l : bool := existsb (str_eq el) l.

Definition extend (i: Iota) (x: Xe) (ts: Ts) : Iota :=
  match i with
  | (xs, i) =>
    (
      if inb x xs then xs else x :: xs,
      fun y => if str_eq x y then Some ts else i y
    )
  end.

Definition emptyIota : Iota := (nil, fun _ => None).

Definition app (i : Iota) (x : Xe) : option Ts :=
  (snd i) x.

Fixpoint substitute (s : Sigma) (t : Ts) (depth : nat) {struct depth} : Ts :=
  match depth with
  | O => t
  | S n => match t with
           | V _ x => match lookup s x with
                      | None => t
                      | Some tx => substitute s tx n
                      end
           | C _ m ts => C _ m (map (fun ts' => substitute s ts' n) ts)
           end
  end.

Fixpoint substituteGoal (s : Sigma) (g : G Se) (depth: nat) : option (G Se) :=
  match g with
  | Invoke _ name as' => Some (Invoke _
                                      name
                                      (map
                                         (fun s' => substitute s s' depth)
                                         as'
                              ))
  | _ => None
  end.

Definition substitueConjs s := map (substituteGoal s).

Definition list_to_set {A} (l : list A) : set A := l.

Definition o (sigma theta : Sigma) : option Sigma :=
  (* TODO: check for disjoin *)
  Some (map (fun sts => (fst sts, substitute sigma (snd sts) 100)) (sigma ++ theta)).

Definition re_def {A B} (d: Def A) : Def B :=
  match d with
  | def _ a b c => def B a b c
  end.

Definition opt_to_list {A} (o : option A) : list A :=
  match o with
  | Some x => x :: nil
  | None  => nil
  end.

Require Import Coq.Program.Basics.

Definition mplus {A} (a b : list A) : list A :=
  match (a, b) with
  | (h1 :: t1, h2 :: t2) => a ++ b
  | (_, _) => nil
  end.

Definition bind_list {A B} (a : list A) (f : A -> list B) : list B :=
  concat (map f a).

Notation " a >>= f " := (bind_list a f)
                          (at level 10, left associativity).


Definition update (gamma : Gamma) (def' : Def _) : Gamma :=
  match gamma with
  | (p, i, d) =>
    match def' with
    | def _ name _ _ =>
      ((extend_dict p name def'), i, d)
    end
  end.

Definition interpret_list (iota : Iota) (tx : list Tx) : option (list Ts) :=
   opt_list_to_list_opt nil (map (interpret iota) tx).

Fixpoint pre_eval (vars: list Se) (g : Gamma) (goal : G Xe) :
  option (G Se * Gamma * list Se) :=
    match goal with
    | Uni _ t1 t2 => match g with
                     | (p, i, d) => match (i <@> t1), (i <@> t2) with
                                    | Some t1', Some t2' => Some (Uni _ t1' t2', g, vars)
                                    | _, _ => None
                                    end
                     end
    | Conj _ g1 g2 => match pre_eval vars g g1 with
                      | Some (g1', g', vars') => match pre_eval vars' g' g2 with
                                                 | Some (g2', g'', vars'') => Some (Conj _ g1' g2', g'', vars'')
                                                 | None => None
                                                 end
                      | None => None
                      end
    | Disj _ g1 g2 => match pre_eval vars g g1 with
                      | Some (g1', g', vars') => match pre_eval vars' g' g2 with
                                                 | Some (g2', g'', vars'') => Some (Disj _ g1' g2', g'', vars'')
                                                 | None => None
                                                 end
                      | None => None
                      end
    | Fresh _ x g' => match g with
                      | (p, i, d) =>
                        pre_eval (d :: vars)
                                 (p, extend i x (V _ d), (new_se d))
                                 g'
                      end
    | Invoke _ f fs =>
      match g with 
      | (_, i, _) => option_map (fun fs' => (Invoke _ f fs', g, vars))
                                (interpret_list i fs)
      end
    | Let _ def' g'  => match pre_eval vars g g' with
                        | Some (g'', e', vars') =>
                          Some (Let Se (re_def def') g'', e', vars')
                        | None => None
                        end
    end.

Fixpoint eval (env : Gamma) (s : Sigma) (g: G Se) (depth: nat) {struct depth}
  : list (Sigma * Delta) :=
  match depth with
    | S n =>
      match env with
      | (p, i, d) =>
        match g with
        | Uni _ t1 t2  => compose
                            opt_to_list
                            (option_map (fun r => (r, d)))
                            (unify (Some s) t1 t2 n)
        | Disj _ g1 g2 => mplus (eval env s g1 n) (eval env s g2 n)
        | Conj _ g1 g2 => (eval env s g1 n) >>=
                                          (fun sd => match sd with
                                                     | (s', d') =>
                                                       eval (p, i, d') s' g2 n
                                                     end)
        | Invoke _ f as' =>
          match lookup_dict p f with
          | Some deff =>
            let (_, fs, g0) := deff in
            let i' : Iota := fold_left (fun i' fa  => extend i' (fst fa) (snd fa))
                                       (combine fs as')
                                       i in
            match pre_eval nil (p, i', d) g0 with
            | Some (g1, env', _) => eval env' s g1 n
            | None => nil
            end
          | None => nil
          end
        | Let _ def' g => eval (update env (re_def def')) s g n
        | Fresh _ _ _ => nil (* cannot happen *)
        end
      end
    | O => nil
  end.

(* Open Scope list_scope. *)

Fixpoint remove_str (x : string) (l : list string) : list string :=
  match l with
  | nil => nil
  | y :: tl => if (str_eq x y) then remove_str x tl else y :: (remove_str x tl)
  end.

Open Scope string_scope.

Example remove_str_ex1 : remove_str "1" ["1"; "2"; "3"] = ["2"; "3"].
Proof. compute. tauto. Qed.

Definition str_list_diff := fold_left (flip remove_str).

Fixpoint str_list_diff' (one two : list string) : list string :=
  match two with
  | h::t => str_list_diff' (remove_str h one) t
  | [] => one
  end.
Example str_list_diff_ex1 : (str_list_diff' ["a"; "b"; "c"] ["a"]) = ["b"; "c"].
Proof. compute. tauto. Qed.

(* Theorem str_list_diff_does_not_occur : forall l1 l2 l3, *)
(*     str_list_diff l1 l2 = l3 -> forall l, In l l2 -> ~ In l l3. *)
(* intros.  *)

Fixpoint fv (t : Term Xe) {struct t} : list Xe :=
  match t with
  | V _ v =>  v :: nil
  | C _ _ ts => concat (map fv ts)
  end.

Fixpoint fvg (g : G Xe) : list Xe :=
  match g with
  | Uni _ t1 t2 =>
    (fv t1)
      ++ (fv t2)
  | Conj _ g1 g2 => fvg g1 ++ fvg g2
  | Disj _ g1 g2 => fvg g1 ++ fvg g2
  | Invoke _ _ ts => concat (map fv ts)
  | Fresh _ x g => str_list_diff (fvg g) [x]
  | Let _ _ g => fvg g
  end.

Fixpoint post_eval' (vars : list string) goal :=
  match goal with
  | Let _ def'  g =>
    let (f, args, b) := def' in
    let freshs : list string := str_list_diff (str_list_diff (fvg b) args) vars in
    let goals := fold_right (fun x g => Fresh _ x g)
                            (post_eval' (vars ++ args ++ freshs) b)
                            freshs in
    Let _ (def _ f args goals) (post_eval' vars g)
  | Conj _ g1 g2 => Conj _ (post_eval' vars g1) (post_eval' vars g2)
  | Disj _ g1 g2 => Disj _ (post_eval' vars g1) (post_eval' vars g2)
  | _ => goal
  end.

Fixpoint post_eval (vars : list Xe) (goal : G Xe) : G Xe :=
  let freshs := str_list_diff (fvg goal) vars in
  fold_right (fun x g => Fresh _ x g)
             (post_eval' (freshs ++ vars) goal)
             freshs.

Definition env0 : Gamma := (nil, emptyIota, 0).

Definition run (goal : G Xe) (depth : nat) : list Sigma :=
  match pre_eval nil env0 goal with
  | Some (goal', env', _) => map fst (eval env' nil goal' depth)
  | None => nil
  end.

Definition zero A : Term A := C A "O" nil.
Definition succ A x := C A "S" [x].

Definition x_2_g := Fresh Xe
                          "x"
                          (Uni Xe (V Xe "x")
                               (zero Xe)).
Compute run x_2_g 10.